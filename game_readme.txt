
■休止中のEC2およびRDSの復元の方法

EC2
・停止中のインスタンスを起動し直す。
・vsftpdが止まっている場合があるので起動する

RDS
・スナップショットから復元する
・game-dbinstance-1-final-snapshot
・復元の際に、名称とセキュリティグループに気を付ける。
・名称はそのままエンドポイントにつながる

・EC2内部のsetting.phpの中にあるendpointと復元したRDSのエンドポイントを合わせる